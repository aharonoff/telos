var talisman = [
  [16, 5, 9, 4],
  [3, 10, 6, 15],
  [2, 11, 7, 14],
  [13, 8, 12, 1]
]

var texts = {
  font: null,
  intro: "A talisman is an occult object stemming from astrological practices. It connects the possessor with the spiritual world to provide functions such as healing and protection. The square associated with Jupiter appears as a talisman used to drive away melancholy."
}

var board = {
  x: null,
  y: null
}

var colors = {
  black: [0, 0, 0],
  white: [255, 255, 255],
  gray: [128, 128, 128],
  red: [255, 0, 0]
}

var states = {
  intro: true,
  telos: false,
  matrix: [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0]
  ],
  open: false
}

var links = [
  "https://huktamaki.gitlab.io/dtime",
  "https://huktamaki.gitlab.io/caver",
  "https://huktamaki.gitlab.io/human",
  "https://huktamaki.gitlab.io/cword",
  "https://huktamaki.gitlab.io/skinc",
  "https://huktamaki.gitlab.io/dgest",
  "https://huktamaki.gitlab.io/autop",
  "https://drive.google.com/file/d/1j_GRilxi5z8SI_Pqla9NHFIZ_gzRSCAA/view?usp=sharing",
  "https://huktamaki.gitlab.io/quggl",
  "https://huktamaki.gitlab.io/lethe",
  "https://huktamaki.gitlab.io/shred",
  "https://huktamaki.gitlab.io/techf",
  "https://huktamaki.gitlab.io/hddna",
  "https://huktamaki.gitlab.io/virus",
  "https://huktamaki.gitlab.io/fussd",
  "https://huktamaki.gitlab.io/evolv"
]

function preload() {
  texts.font = loadFont('ttf/texturina.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    board.y = windowHeight * 0.95
    board.x = board.y
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(colors.white)
  rectMode(CENTER)
  textFont(texts.font)

  if (states.intro === true) {
    fill(colors.black)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, board.x, board.y)

    textAlign(LEFT, CENTER)
    textSize(board.x * 0.07)
    fill(colors.white)
    noStroke()
    text(texts.intro, windowWidth * 0.5, windowHeight * 0.5 - board.y * 0.025, board.x * 0.9, board.y)
  }

  if (states.telos === true) {
    for (var i = 0; i < talisman.length; i++) {
      for (var j = 0; j < talisman[i].length; j++) {
        if (mouseX >= windowWidth * 0.5 - board.x * 0.5 && mouseX <= windowWidth * 0.5 + board.x * 0.5 && mouseY >= windowHeight * 0.5 - board.y * 0.5 && mouseY <= windowHeight * 0.5 + board.y * 0.5) {
          fill(colors.gray)
          noStroke()
          rect(windowWidth * 0.5 + (Math.floor(dist(windowWidth * 0.5 - board.x * 0.5, 0, mouseX, 0) / (board.x * 0.25)) - (talisman.length - 1) * 0.5) * board.x * 0.25, windowHeight * 0.5 + (Math.floor(dist(windowHeight * 0.5 - board.y * 0.5, 0, mouseY, 0) / (board.y * 0.25)) - (talisman[i].length - 1) * 0.5) * board.y * 0.25, board.x * 0.25, board.y * 0.25, board.x * 0.025)
        }
      }
    }

    for (var i = 0; i < talisman.length; i++) {
      for (var j = 0; j < talisman[i].length; j++) {
        fill(colors.black)
        noStroke()
        rect(windowWidth * 0.5 + (i - (talisman.length - 1) * 0.5) * board.x * 0.25, windowHeight * 0.5 + (j - (talisman[i].length - 1) * 0.5) * board.y * 0.25, board.x * 0.225, board.y * 0.225, board.x * 0.05)

        textAlign(CENTER, CENTER)
        textSize(board.x * 0.15)
        if (states.matrix[i][j] === 0) {
          fill(colors.white)
          noStroke()
          text(talisman[i][j], windowWidth * 0.5 + (i - (talisman.length - 1) * 0.5) * board.x * 0.25, windowHeight * 0.5 + (j - (talisman[i].length - 1) * 0.5) * board.y * 0.25 - board.y * 0.05)
        } else {
          fill(colors.red)
          noStroke()
          ellipse(windowWidth * 0.5 + (i - (talisman.length - 1) * 0.5) * board.x * 0.25, windowHeight * 0.5 + (j - (talisman[i].length - 1) * 0.5) * board.y * 0.25, board.x * 0.1, board.x * 0.1)
        }
      }
    }
  }
}

function mousePressed() {
  if (states.intro === true) {
    setTimeout(function() {
      states.intro = false
      states.telos = true
    }, 150)
  }

  if (states.telos === true) {
    if (mouseX >= windowWidth * 0.5 - board.x * 0.5 && mouseX <= windowWidth * 0.5 + board.x * 0.5 && mouseY >= windowHeight * 0.5 - board.y * 0.5 && mouseY <= windowHeight * 0.5 + board.y * 0.5) {
      if (states.matrix[Math.floor(dist(windowWidth * 0.5 - board.x * 0.5, 0, mouseX, 0) / (board.x * 0.25))][Math.floor(dist(windowHeight * 0.5 - board.y * 0.5, 0, mouseY, 0) / (board.y * 0.25))] === 0) {
        states.matrix[Math.floor(dist(windowWidth * 0.5 - board.x * 0.5, 0, mouseX, 0) / (board.x * 0.25))][Math.floor(dist(windowHeight * 0.5 - board.y * 0.5, 0, mouseY, 0) / (board.y * 0.25))] = 1
        states.open = true
      }
    }
  }
}

function mouseReleased() {
  if (states.telos === true) {
    if (states.open === true) {
      setTimeout(function() {
        for (var i = 0; i < states.matrix.length; i++) {
          for (var j = 0; j < states.matrix[i].length; j++) {
            if (states.matrix[i][j] === 1) {
              window.open(links[talisman[i][j] - 1], '_blank')
            }
          }
        }
        states.open = false
        states.matrix = [
          [0, 0, 0, 0],
          [0, 0, 0, 0],
          [0, 0, 0, 0],
          [0, 0, 0, 0]
        ]
      }, 250)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    board.y = windowHeight * 0.95
    board.x = board.y
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x
  }
  createCanvas(windowWidth, windowHeight)
}
